package org.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PassingMultipleDataLoginApache {
	public static void main(String[] args) throws IOException {
		 WebDriver driver=new ChromeDriver();
		    driver.manage().window().maximize();
		    driver.get("https://demowebshop.tricentis.com/");
		    File f = new File("/home/ajeez/Desktop/testdata/testdata2.xlsx");
		     FileInputStream fs=new FileInputStream(f);
		    XSSFWorkbook workbook =new XSSFWorkbook(fs);
		    XSSFSheet sheet=workbook.getSheetAt(0);
		    int rows=sheet.getPhysicalNumberOfRows();
		    for (int i = 1; i < rows; i++) {
		    	String username=sheet.getRow(i).getCell(0).getStringCellValue();
		    	String password=sheet.getRow(i).getCell(1).getStringCellValue();
		    	driver.findElement(By.linkText("Log in")).click();
				driver.findElement(By.id("Email")).sendKeys(username);
				driver.findElement(By.id("Password")).sendKeys(password);
				driver.findElement(By.xpath("//input[@value='Log in']")).click();
				driver.findElement(By.linkText("Log out")).click();
				
			}
	}
}
